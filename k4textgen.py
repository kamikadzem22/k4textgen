import argparse
import random
import pickle
import re

""" Generates text based on input files """


class MarkovGenerator:
    def __init__(self, n, max):
        """

        :param n:   specifies order for Markov model
                    0 - each letter it independent
                    k - probability depends on k previous consecutive letters (k-gram)
        :param max: number of k-grams that would be generated
        """
        self.order = int(n)
        self.max = int(max)
        self.ngrams = {}
        self.beginnings = []

    def clean_text(self, text):
        return re.sub(r'\'|\"', '', text)

    def build_chain(self, text):
        text = self.clean_text(text)
        if len(text) < self.order:
            raise Exception('Not enough data for desired number')
        beginning = text[0:self.order]
        self.beginnings.append(beginning)
        for i in text.split('\n'):
            self.beginnings.append(i[0:self.order])
        for i in range(len(text) - self.order):
            gram = text[i:i + self.order]
            next = text[i + self.order]

            if gram not in self.ngrams:
                self.ngrams[gram] = []
            self.ngrams[gram].append(next)
        return self.ngrams

    def generate(self):
        current = random.choice(self.beginnings)
        output = current
        for i in range(self.max):
            if current in self.ngrams:
                p_next = self.ngrams[current]
                next = random.choice(p_next)
                output += next
                current = output[-self.order: len(output)]
            else:
                break
        return '\n'.join([i.strip() for i in output.split('\n')])

    def build_model(self, path):
        pickle.dump((self.ngrams, self.beginnings, self.order), open(path, 'wb'))

    def load_model(self, path):
        self.ngrams, self.beginnings, self.order = pickle.load(open(path, 'rb'))


def load_texts(files):
    data = []
    for filename in files:
        with open(filename, 'r') as f:
            data.append((f.read()))
            f.close()
    return data


def main(args):
    gen = MarkovGenerator(
        n=args.number[0] if args.number else 3,
        max=args.max[0]
    )
    if not args.load_model:
        texts = load_texts(args.file_names)
        gen.build_chain('\n'.join(texts))
    else:
        gen.load_model(args.file_names[0])
    text = gen.generate()
    with open(args.output[0], 'w') as f:
        f.write(text)
    if args.build_model:
        gen.build_model(args.build_model[0])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Primitive text processing utility')
    parser.add_argument(
        nargs='+',
        metavar='F',
        dest='file_names',
        help="File names separated by space containing texts for analyzing (they will be joined using line separator)"
    )
    parser.add_argument(
        '-o',
        '--output',
        required=True,
        nargs=1,
        help='Specify location for output'
    )
    parser.add_argument(
        '-n',
        '--number',
        type=int,
        nargs=1,
        help='Specify order for generator. Default is 3'
    )
    parser.add_argument(
        '-m',
        '--max',
        type=int,
        nargs=1,
        required=True,
        help='Maximum length'
    )
    parser.add_argument(
        '--build-model',
        dest='build_model',
        nargs=1,
        help="Specify path for a model and build one"
    )
    parser.add_argument(
        '--load-model',
        dest='load_model',
        action='store_true',
        help="Load model instead of text file. Only first supplied model will be read."
    )
    args = parser.parse_args()
    main(args)
