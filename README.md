# Primitive text generation utility
## Created for Tinkoff Generation Contest

Supplied model was trained using content made by subscribors of [vk.com/baneks](vk.com/baneks). Posts were scraped using [vk-group-scraper](https://gitlab.com/kamikadzem22/vk-group-scraper)


```
usage: k4textgen.py [-h] -o OUTPUT [-n NUMBER] -m MAX
                    [--build-model BUILD_MODEL] [--load-model]
                    F [F ...]

Primitive text processing utility

positional arguments:
  F                     File names separated by space containing texts for
                        analyzing (they will be joined using line separator)

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Specify location for output
  -n NUMBER, --number NUMBER
                        Specify order for generator. Default is 3
  -m MAX, --max MAX     Maximum length
  --build-model BUILD_MODEL
                        Specify path for a model and build one
  --load-model          Load model instead of text file. Only first supplied
                        model will be read.


```